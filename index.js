
// spreading

/*let tableau = [1, 2, 3, 4];
let tableau2 = [5, 6, 7, 8];
let tableau3 = [];
console.log(tableau, tableau2);

tableau3 = [...tableau, ...tableau2];
console.log(tableau3);


let personne = {
  adresse: {
    rue: 455,
    ville: "Abidjan"
  },
  code: {
    zip: 233
  }
}

let { code } = personne;
let { zip } = code
console.log(zip)*/


/* 1 - Welcome.In this kata, you are asked to square every digit of a number and concatenate them.

For example, if we run 9119 through the function, 811181 will come out, because 92 is 81 and 12 is 1.

Note: The function accepts an integer and returns an integer*/

/*function squareDigits(num) {
  let numberString = num.toString();

  let total = "";

  for (let i in numberString) {
    total += (parseInt(numberString[i]) ** 2 + "");

  }
  return parseInt(total);
}

console.log(squareDigits(9119));*/


/*Écrivez une fonction qui
renverra le nombre de caractères alphabétiques et de chiffres numériques distincts
insensibles à la casse qui apparaissent plus d'une fois dans la chaîne d'entrée.
La chaîne d'entrée peut être supposée contenir uniquement des alphabets
(majuscules et minuscules) et des chiffres numériques.*/


/*function duplicateCount(text) {
  let newText = text.toLowerCase();
  let obj = {};


    for (let index = 0; index < newText.length; index++) {
      obj[newText[index]] = newText[index];
    }
    return Object.keys(obj).length;


}
console.log(duplicateCount("Indivisibilities")); */

//const element = array[index];
/*if (!obj[newText[index]]) { // la condition: si ce n'est pas défini ajoute le à l'objet
  obj[newText[index]] = 1;
}
else if (obj[newText[index]] < 2) {
  obj[newText[index]] += 1;
  count++;
}*/

// Étant donné un tableau d'entiers, votre solution devrait trouver le plus petit entier.

/* class SmallestIntegerFinder {
    findSmallestInt(args) {
      return Math.min(...args);
    }
  }*/

/*
Return the number (count) of vowels in the given string.

We will consider a, e, i, o, u as vowels for this Kata (but not y).

The input string will only consist
of lower case letters and/or spaces.*/


/*function getCount(str) {

  // return (str.match(/[aeiou]/ig)||[]).length;
  let vowCount = 0;
  let vowels = ['a', 'e', 'i', 'o', 'u']
  for (let char of str) {
    if (vowels.includes(char.toLowerCase())) {
      vowCount++;
    }
  }
  return vowCount
}

console.log(getCount("abracadabra"));*/

/*
Implement a function that accepts 3 integer values a, b, c.
The function should return true if a triangle can be built with the sides
of given length and false in any other case.

(In this case, all triangles must have
  surface greater than 0 to be accepted).*/


/*function isTriangle(a, b, c) {
  //  return a + b > c && a + c > b && c + b > a; (en une seule ligne )
  if (a <= 0 || b <= 0 || c <= 0) {
    return false;
  }

  if (a+b > c && a+c > b && b+c > a) {
    return true;

  }else
  return false;
}

console.log(isTriangle(2,2,2));*/

/*
Créez une fonction prenant un entier positif comme paramètre
et renvoyant une chaîne contenant la représentation en chiffres
romains de cet entier.*/
/*function solution(number) {
  // convert the number to a roman numeral

  let map = {
    M: 1000,
    CM: 900,
    D: 500,
    CD: 400,
    C: 100,
    XC: 90,
    L: 50,
    XL: 40,
    X: 10,
    IX: 9,
    V: 5,
    IV: 4,
    I: 1,
  };

  let result = '';
  for (const key in map) {
  const repeatCounter = Math.floor(num / map[key]);
  }
}*/


/* our goal in this kata is to implement a difference function, which subtracts one list from another and returns the result.

It should remove all values from list a, which are present in list b keeping their order.*/

/*function arrayDiff(a, b) {
  return a.filter(x => !b.includes(x));
}*/


/* This time we want to write calculations using functions and get the results. Let's have a look at some examples:

seven(times(five())); // must return 35
four(plus(nine())); // must return 13
eight(minus(three())); // must return 5
six(dividedBy(two())); // must return 3 */


/*function zero(func) { return func ? func(0) : 0; }; // si la fonction existe elle sera résolu avec zero sinon c'est égal à zero.
function one(func) { return func ? func(1) : 1; }; //const one = a => a ? a(1) : 1
function two(func) { return func ? func(2) : 2; };
function three(func) { return func ? func(3) : 3; };
function four(func) { return func ? func(4) : 4; };
function five(func) { return func ? func(5) : 5; };
function six(func) { return func ? func(6) : 6; };
function seven(func) { return func ? func(7) : 7; };
function eight(func) { return func ? func(8) : 8; };
function nine(func) { return func ? func(9) : 9; };

function plus(a) {
  return function (b) { return b + a; } // function plus(a) {return (b)=>b+a}
}
function minus(a) { return function (b) { return b - a; } } // function plus(a) {return (b)=>b-a}
function times(a) { return function (b) { return a * b; } } // function times(a) {return (b)=> a * b}
function dividedBy(a) { return function (b) { return b / a; } } // function dividedBy(a) {return (b)=>Math.floor(a/b)}*/



/* Write a function that accepts an array of 10 integers (between 0 and 9), that returns a string of those numbers in the form of a phone number.

Example
createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]) // => returns "(123) 456-7890"*/

/*function createPhoneNumber(numbers) {
  let part1 = "";
  let part2 = "";
  let part3 = "";
  for (let index = 0; index < numbers.length; index++) {
    if (index < 3) {
      part1 += numbers[index].toString()
    } else if (index >= 3 && index < 6) {
      part2 += numbers[index].toString()
    } else if (index >= 6) {
      part3 += numbers[index].toString()
    }
  }
  return `(${part1}) ${part2}-${part3}`

}
console.log(createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]));


function createPhoneNumber(numbers) {
  var format = "(xxx) xxx-xxxx";

  for (var i = 0; i < numbers.length; i++) {
    format = format.replace('x', numbers[i]);
  }

  return format;
}*/


/*function solution(string) {
  let stringSplit = string.split("");
  let newString = "";
  stringSplit.forEach(element => {
    if (element === element.toUppercase()) {
      newString += " " + element;
    } else newString += element;
  });
  return newString;
}

/* Complete the solution so that the function will break up camel casing, using a space between words.

Example
"camelCasing"  =>  "camel Casing"
"identifier"   =>  "identifier"
""             =>  "" */

/*function solution(string) {
  return string.replace(/[A-Z]/g, " $&");
}

console.log(solution("camelCasing"));*/

/*function solution(string) {
  string = string.split('').map(function (el) {
    if (el === el.toUpperCase()) {
      el = ' ' + el
    }
    return el
  })
  return string.join('')
}*/

/* Un nombre narcissique (ou nombre d'Armstrong) est un nombre positif qui est la somme de ses propres chiffres, chacun élevé à la puissance du nombre de chiffres dans une base donnée. Dans ce Kata, nous nous limiterons au décimal (base 10).

Prenons par exemple 153 (3 chiffres), qui est narcissique :

    1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153*/

function narcissistic(value) {
  let x = 0
  let pow = 0
  if (value < 9) {
    x = value.toString().split("")
    for (let index = 0; index < x.length; index++) {
      pow += Math.pow(Number(x[index]), x.length)
    }
    return pow === value ? true : false
  } else {

    return true
  }
}

console.log(narcissistic(137));
